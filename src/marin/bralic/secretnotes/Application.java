package marin.bralic.secretnotes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Application extends Activity{
	private List<Item> dataList;
	private SArrayAdapter listAdapter;
	private ListView listView;
	private boolean selectMod;	
	private int openedItem;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String password=loadPasswordData();
		if(password==null){
			setContentView(R.layout.first_start_layout);
			EditText edt=(EditText)findViewById(R.id.edt1);
			edt.requestFocus();
		    getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		}else{
			setContentView(R.layout.start_layout);
			EditText edt=(EditText)findViewById(R.id.edt1);
			edt.requestFocus();
		    getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		}
	}
	
	public void setNewPassword(View view){
		TextView t1=(TextView)findViewById(R.id.edt1);
		TextView t2=(TextView)findViewById(R.id.editText2);
		
		if(t1.getText().toString().equals(t2.getText().toString()) && !t1.getText().toString().equals("")){
			savePasswordData(""+t1.getText());
			
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(t1.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(t2.getWindowToken(), 0);
			
			setContentView(R.layout.app_layout);
			fillListView();	
		}else{
			Toast.makeText(this, "Passwords are different.", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void startApplication(View view){
		TextView t=(TextView)findViewById(R.id.edt1);
		String password=loadPasswordData();
		
		if(t.getText().toString().equals(password)){
	        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(t.getWindowToken(), 0);
			setContentView(R.layout.app_layout);
			fillListView();
		}else{
			Toast.makeText(this, "Wrong password.", Toast.LENGTH_SHORT).show();
		}
	}
			
	private void fillListView(){
		loadData();
		
		listAdapter = new SArrayAdapter(this, R.layout.list_layout, dataList);	      
		listView=(ListView)findViewById(R.id.listView1);
		listView.setAdapter(listAdapter);
		listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Item pom=dataList.get(arg2);
				
				if(selectMod){					
					pom.selected=!pom.selected;					
					listAdapter.notifyDataSetChanged();
				}else{
					String data=pom.data;
					setContentView(R.layout.item_layout);
					EditText edt=(EditText)findViewById(R.id.edt1);
					edt.setText(data);
					openedItem=arg2;
				}
				
			}			
		});
		
	}
	
	public void exit(View view){
		finish();
	}
	
	
	
	//application layout buttons
	public void selectMod(View view){
		selectMod=!selectMod;
		
		Button btn=(Button)findViewById(R.id.buttonChangePassword);
		Button btn2=(Button)findViewById(R.id.buttonSelect);
		Button btn3=(Button)findViewById(R.id.buttonNewOrBackup);
		Button btn4=(Button)findViewById(R.id.buttonAll);
		
		if(selectMod){
			for(int i=0;i<dataList.size();++i) dataList.get(i).selected=false;
			
			btn.setText("Delete");
			btn.setBackgroundColor(Color.parseColor("#B00000"));

			btn2.setText("Select");
			btn2.setBackgroundColor(Color.parseColor("#FFA000"));
		 	
			btn3.setText("To .txt");
			btn3.setBackgroundColor(Color.parseColor("#9000FF"));
			
			btn4.setText("All");
			btn4.setBackgroundColor(Color.parseColor("#FFA000"));
		}else{
			listAdapter.notifyDataSetChanged();
			btn.setText("ChPass");
			btn.setBackgroundColor(Color.parseColor("#20B020"));
			
			btn2.setText("Select");
			btn2.setBackgroundColor(Color.parseColor("#5050FF"));
			
			btn3.setText("New");
			btn3.setBackgroundColor(Color.parseColor("#FFA000"));
			
			btn4.setText("");
			btn4.setBackgroundColor(Color.parseColor("#500050"));
		}
	}
	
	public void chPassOrDell(View view){
		int s=0;
		for(int i=0;i<dataList.size();++i) if(dataList.get(i).selected) ++s;
		
		if(selectMod && s!=0){
			showDeleteDialog();
		}else if(!selectMod){
			setContentView(R.layout.change_password_layout);
			
			EditText et=(EditText)findViewById(R.id.edt1);
			et.requestFocus();
		}
	}
	
	public void newItemOrBackup(View view){
		if(selectMod){
			String backupData="";
			
			for(int i=0;i<dataList.size();++i)
				if(dataList.get(i).selected) backupData+=dataList.get(i).data+"\n\n";
				
			 try {
				 File root = new File(Environment.getExternalStorageDirectory(), "Notes");
				 if(!root.exists()){
			          root.mkdirs();
			     }
			     File gpxfile = new File(root, "SecretNotesBackup"+System.currentTimeMillis()+".txt");
			     FileWriter writer = new FileWriter(gpxfile);
			     writer.write(backupData);
			     writer.flush();
			     writer.close();
			     
			     Toast.makeText(this, "Backup finish.", Toast.LENGTH_SHORT).show();
			        
			 }catch (IOException e) {
			    Toast.makeText(this, "Write to file error.", Toast.LENGTH_SHORT).show();
			 } 
			 
			 selectMod(null);
		}else{
			selectMod=false;
			openedItem=dataList.size();
			setContentView(R.layout.item_layout);
			
			EditText edt= (EditText) findViewById(R.id.edt1);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
		}		
	}

	public void selectAll(View view){
		if(selectMod){
			int s=0;
			for(int i=0;i<dataList.size();++i){
				if(dataList.get(i).selected) ++s; 
			}
			
			if(s==dataList.size()){
				//disselect all
				for(int i=0;i<dataList.size();++i){
					dataList.get(i).selected=false;
				}
			}else{
				//select all
				for(int i=0;i<dataList.size();++i){
					dataList.get(i).selected=true;
				}
			}
			
			listAdapter.notifyDataSetChanged();
		}		        
	}
	
	private void showDeleteDialog(){
	     AlertDialog.Builder builder = new AlertDialog.Builder(this);
	     builder.setTitle("Are you sure you want to delete items ?");
	     builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int id) {
	        	 
	        	String[] pom=new String[dataList.size()];
	 			for(int i=0;i<dataList.size();++i){
	 				if(!dataList.get(i).selected) pom[i]=dataList.get(i).data;
	 				else pom[i]=null;
	 			}
	 			dataList.clear();
	 			for(int i=0;i<pom.length;++i){
	 				if(pom[i]!=null) dataList.add(new Item(pom[i], i));
	 			}
	 			
	 			commitData();
	 			
	 			selectMod(null);
	 			
	 			listAdapter.notifyDataSetChanged();
	            dialog.dismiss();	            
	         }
	     });
	     builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int id) {
	              dialog.dismiss();
	         }
	     });
	     AlertDialog dialog = builder.create();
	     dialog.show();
	   }
	
		  
	
	//change password buttons
	public void changePassword(View view){
		TextView oldPassword=(TextView)findViewById(R.id.edt1);
		TextView newPassword1=(TextView)findViewById(R.id.edt2);
		TextView newPassword2=(TextView)findViewById(R.id.edt3);
		
		String oldPass=loadPasswordData();
		String insertedOldPass=""+oldPassword.getText();
		String newPass1=""+newPassword1.getText();
		String newPass2=""+newPassword2.getText();
		
		if(!oldPass.equals(""+insertedOldPass)) Toast.makeText(this, "Wrong old password.", Toast.LENGTH_SHORT).show();
		else if(!newPass1.equals(newPass2)) Toast.makeText(this, "New passwords are different..", Toast.LENGTH_SHORT).show();
		else{		
			savePasswordData(newPass2);
			close(null);
			Toast.makeText(this, "Password changed..", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	//item layout buttons
	public void saveItem(View view){
		EditText edText=(EditText)findViewById(R.id.edt1);
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edText.getWindowToken(), 0);		
		
		if(edText.getText().toString().equals("")) return;
				
		if(openedItem==dataList.size()) dataList.add(new Item(edText.getText().toString(), openedItem));
		else dataList.get(openedItem).data=edText.getText().toString();
		
		commitData();
		setContentView(R.layout.app_layout);
		fillListView();
	}
	
	public void close(View view){
		try{
			EditText edText1=(EditText)findViewById(R.id.edt1);
			EditText edText2=(EditText)findViewById(R.id.edt2);
			EditText edText3=(EditText)findViewById(R.id.edt3);
			
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edText1.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(edText2.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(edText3.getWindowToken(), 0);
		}catch(Exception e){}
		
		
		setContentView(R.layout.app_layout);
		fillListView();
	}
		
	
	//load, store
	private void savePasswordData(String key){
		SharedPreferences sharedData = getSharedPreferences("SHARED_DATA", MODE_PRIVATE);
		SharedPreferences.Editor editor=sharedData.edit();
		editor.putString("PASSWORD", key);
		editor.commit();
	}
	
	private String loadPasswordData(){
		SharedPreferences data = getSharedPreferences("SHARED_DATA", MODE_PRIVATE);
		return data.getString("PASSWORD", null);
	}
	
	private void loadData(){
		SharedPreferences sharedData = getSharedPreferences("SHARED_DATA", MODE_PRIVATE);
		
        if(dataList!=null) dataList.clear();
		
		dataList=new LinkedList<Item>();
		String data;
		int i=0;
				
		while((data=sharedData.getString("DATA"+i, null))!=null){
			dataList.add(new Item(data, i));
			++i;
		}
		
	}
	
	private void commitData(){	
		SharedPreferences sharedData = getSharedPreferences("SHARED_DATA", MODE_PRIVATE);
		String password=sharedData.getString("PASSWORD", null);
		SharedPreferences.Editor editor=sharedData.edit();
	
		editor.clear();
		editor.commit();
		
		for(int i=0;i<dataList.size();++i) editor.putString("DATA"+i, dataList.get(i).data);
		
		editor.putString("PASSWORD", password);		
		editor.commit();
	}
	
	@SuppressLint("ViewHolder")
	private class SArrayAdapter extends ArrayAdapter<Item>{
		private final Context context;
		private final List<Item> list;
		
		public SArrayAdapter(Context context, int resource, List<Item> objects) {
			super(context, resource, objects);
			this.context=context;
			list=objects;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {			
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View view = inflater.inflate(R.layout.list_layout, parent, false);
			
		    TextView itemText=(TextView)view.findViewById(R.id.itemText);
		    if(list.get(position).selected && selectMod) itemText.setBackgroundColor(Color.parseColor("#F09000"));
		    else itemText.setBackgroundColor(Color.BLACK);
		    
		    String s=list.get(position).data, s2="";
		    char[] c=s.toCharArray();
		    int n=0, i, j;
		    
		    for(i=0, j=0;i<c.length;++i, ++j){
		    	if(((int)c[i])==10){
		    		++n;
		    		j=0;
		    	}else if(j>18){
		    		++n;
		    		j=0;
		    	}

		    	if(n==2){
		    		s2+="...";
		    		break;
		    	}
		    	s2+=c[i];
		    }
		    		    		    
		    itemText.setText(s2);		    
			return view;
		}			
		
	}
}
