package marin.bralic.secretnotes;

public class Item {
	public String data;
	public boolean selected, show;
	private long key;

	
	public Item(String data, long key){
		this.data=data;
		this.key=key;
		show=true;
	}
	
	public long getKey(){
		return key;
	}
	
	public boolean equals(Item item2){
		return item2.getKey()==this.key;
	}
	
}
